#!/usr/bin/env python3

# todo actually use the logger / substitute possible sys.std* writes
# todo automatically reduce indexing threads when indexing fails due to RAM requirements
# todo don't reload already downloaded and verified files
# todo automatically distinguish between new and old annotation formats
# todo check for uniqueness of entries somewhere or maybe even join annotations with the same sequence?
# todo check internet access and proxy settings
# todo add handling of merged taxonomic nodes
# possibly decouple / thread input / output from calculations or maybe use aysnc syntax

import config
import gzip
import fcntl
import os
import re
import subprocess
import sys
import multiprocessing
import shutil
import logging
import itertools
import time

# URLs - don't need to be changed unless NCBI changes anything / directory urls have to end with / to work correctly
ncbi_ftp_url = 'ftp://ftp.ncbi.nlm.nih.gov/'
ncbi_taxonomy_url = ncbi_ftp_url + 'pub/taxonomy/'
taxdump_url = ncbi_taxonomy_url + 'taxdump.tar.gz'
accession2taxid_folder_url = ncbi_taxonomy_url + 'accession2taxid/'
blast_db_folder_url = ncbi_ftp_url + 'blast/db/'
fasta_db_folder_url = blast_db_folder_url + 'FASTA/'

# global variables
# lock file variable, just here to set a global scope
lock_file = None

# regular expression for defline splitting by accession.version
# this will capture all accession version matches, but is so relaxed, it will probably produce false positives
#acc_ver_re = re.compile(' >(\w+\.\d+) ')
acc_ver_re = re.compile(' >(\w+\.\d+|\w+_\w) ')  # this version includes archaic pdb accession format (ex. 3CC4_9)

# set defining which characters we keep on string cleanup (for sequence descriptions and node names)
keepchars = {'!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4',
             '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
             'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\',
             ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
             'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}'}


class DependencyException(Exception):
    pass


class FailedToExecuteException(Exception):
    pass


def setup_logger(level='DEBUG', name='DB UP'):
    """
    Function creating a logger and setting initial settings.

    :param level: str determining the logging verbosity level
    :param name: str name of the logger
    :return: logging.Logger object designating the root of the logger hierarchy
    """

    # create logger
    logger = logging.getLogger(name)
    logger.setLevel(level.upper())

    # create console handler
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.DEBUG)

    # create formatter to give output in the desired format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to console_handler
    console_handler.setFormatter(formatter)

    # add console_handler to logger
    logger.addHandler(console_handler)

    return logger


def get_file_names_from_ncbi_ftp_directory_url(url, dependencies, temp_dir='/tmp', max_tries=10, ftp_proxy=''):
    """
    Function that downloads an index file of an ftp server directory (to a temp directory) and returns a list with all
    files in it.

    :param url: str URL to an ftp server directory
    :param dependencies: dict {str program: str path}
    :param temp_dir: str path to a temporary directory to use for the download
    :param max_tries: int number designating the number of times to try the download before raising an exception
    :param ftp_proxy: str URL to the FTP proxy to use
    :return: List of URLs to the files contained in the directory
    """

    file_path = download_file(url, temp_dir, dependencies, max_tries, ftp_proxy=ftp_proxy)

    file_urls = []

    with open(file_path) as infile:
        for line in infile:
            if '<a href' in line:
                file_urls.append(os.path.basename(line.split('<a href')[1].split('"')[1]))

    return file_urls


def gather_accession_numbers_from_fasta_gz_file(fasta_gz_file_path):
    """
    Function that reads in a fasta file and gathers all found accession numbers in a dictionary without values and
    returns that dictionary.

    :param fasta_gz_file_path: str path to a gz compressed fasta file with NCBI sequence annotations
    :return: dict {str accession_number: None}
    """

    acc_to_ti_dict = {}
    gen = fasta_gz_file_read_generator(fasta_gz_file_path)
    seq_count = 0
    acc_count = 0

    for read_name, read_sequence in gen:
        seq_count += 1
        for accession, description in split_defline(read_name):
            acc_count += 1
            acc_to_ti_dict[accession] = None
        if seq_count % 1000 == 0:
            sys.stdout.write('Gathered {} accessions from {} sequences\r'.format(acc_count, seq_count))
            sys.stdout.flush()

    sys.stdout.write('Gathered {} accessions from {} sequences\n'.format(acc_count, seq_count))
    sys.stdout.flush()

    return acc_to_ti_dict


def split_defline(defline):
    """
    Function that returns a list of singled out NCBI annotations from a sequence annotation line containing several
    annotations.
    NCBI splits their annotations by '\x01' in the downloadable fasta which is the CTRL+A character by default.
    In the fasta produced by extracting from a blast database they are split by ' >'.
    Since ' >' can occur in single annotations as well we need to split in a more fancy way (like using a regex).

    :param defline: str native NCBI sequence defline containing multiple annotations (or other deflines)
    :return: List of tuples (str accession, str description)
    """


    # format detection and flagging
    blast_format = True
    if '\x01' in defline:  # deflines with only one entry are treated as blast deflines, would work as fasta too
        blast_format = False

    # actual processing
    result = []
    if blast_format:
        re_splittable_defline = ' >'+defline  # adding a space&> so all accessions are bounded by spaces and a >
        temp_acc = ''
        split_done = acc_ver_re.split(re_splittable_defline)
        split_done = [x for x in split_done if x]  # remove empty string at the beginning ... if there is one
        for index, item in enumerate(split_done):
            if index % 2 == 0:  # every even element is an accession number
                temp_acc = item
            else:
                result.append((temp_acc, clean_ncbi_sequence_name(item)))  # clean name before appending
    else:  # fasta format
        for index, item in enumerate(defline.strip().split('\x01')):
            item_split = item.split(' ', maxsplit=1)  # first space marks end of accession, start of description
            result.append((item_split[0], clean_ncbi_sequence_name(item_split[1])))

    # return a list of tuples (str accession, str description)
    return result


def fasta_gz_file_read_generator(fasta_gz_file_path):
    """
    Function returning a generator object that iterates over the given fasta file.
    The generator yields reads from the fasta file as tuples.
    Wrapped sequences are handled appropriately.

    :param fasta_gz_file_path: str path to a gz compressed fasta file to generate reads from
    :return: Generator object yielding (str read_name, str read_sequence)
    """

    seq = []
    rname = ''

    with gzip.open(fasta_gz_file_path, 'rt') as fasta_gz_file:
        for line in fasta_gz_file:
            if line[0] == '>':
                rseq = ''.join(seq)
                if rname:  # only yield when we already have all data for the first read
                    yield rname, rseq
                rname = line.rstrip()[1:]  # omitting the leading >
                seq = []
            else:
                seq += [line.rstrip()]

    rseq = ''.join(seq)
    yield rname, rseq  # don't forget the last read


def check_if_script_is_singleton():
    """
    Function that checks if this is the only instance using of this program running using a lock file
    and raises a BlockingIOError otherwise.

    :return: None
    """

    global lock_file  # has to be global so it doesn't get cleaned up after function exits
    lock_file = open('/var/lock/andys_db_updater_was_here', 'w')
    fcntl.lockf(lock_file, fcntl.LOCK_EX | fcntl.LOCK_NB)  # acquire exclusive lock but don't block if not possible


def check_dependencies(program_dict):
    """
    Function that checks for the availability of certain programs on the system by calling which and raises
    a DependencyException if any of the checks fails.

    :param program_dict: dict {str name: str path to program (may be empty str)}
    :return: dict
    """

    for program, path in program_dict.items():
        if not path:
            error_code, new_path = run_subprocess_with_returncode('which {}'.format(program), True)
            if error_code:
                raise DependencyException('Could not find {} in PATH variable: {}'.format(program, os.environ['PATH']))
            program_dict[program] = new_path[0].rstrip()
        elif not os.path.exists(path):
            raise DependencyException('Could not find {} at {}'.format(program, path))

    return program_dict


def build_taxid_dictionary(nodes_file_path):
    """
    Function that creates a TaxID dictionary {int node_id: [int node_parent_id, str node_name, str node_rank]}
    without names from the nodes.dmp file and returns this dictionary.

    :param nodes_file_path: str path to the nodes.dmp file
    :return: TaxID dictionary {int node_id: [int node_parent_id, str node_name, str node_rank]}
    """

    with open(nodes_file_path) as nodes_file:
        taxid_dict = {}
        counter = 0
        for line in nodes_file:
            counter += 1
            line_split = [x.strip() for x in line.split('|')]
            taxid_dict[int(line_split[0])] = [int(line_split[1]), '', clean_ncbi_sequence_name(line_split[2])]
            if counter % 1000 == 0:
                sys.stdout.write('Read {} nodes\r'.format(counter))
                sys.stdout.flush()

    sys.stdout.write('Read {} nodes\n'.format(len(taxid_dict)))
    sys.stdout.flush()

    # adding a node for 'unknown' organisms
    taxid_dict[0] = [1, 'unknown', 'no_rank']

    return taxid_dict


def add_names_to_taxid_dictionary(names_file_path, taxid_dict):
    """
    Function that adds the names read from names.dmp to the given TaxID dictionary which should be generated by
    build_taxid_dictionary() before.
    The function also cleans up names while parsing and keeps only numbers, letters and hypens and underscores.
    Everything else gets replaced by underscores.

    :param names_file_path: str path to names.dmp file
    :param taxid_dict:  TaxID dictionary as generated by build_taxid_dictionary()
    :return: TaxID dictionary as generated by build_taxid_dictionary() with names for all entries in names.dmp
    """

    with open(names_file_path) as names_file:
        # numbers, capital letters, other letters and - and _
        numread = 0
        numadded = 0

        for line in names_file:
            line_split = [x.strip() for x in line.split('|')]
            taxid = int(line_split[0])
            if line_split[3] == 'scientific name' and taxid in taxid_dict:  # only keep the scientific name for node
                taxid_dict[taxid][1] = clean_ncbi_sequence_name(line_split[1])
                numadded += 1

            numread += 1
            if numread % 1000 == 0:
                percentage = float(numadded)/len(taxid_dict)
                sys.stdout.write('Read {} names / Added {} ({:.2%}) names\r'.format(numread, numadded, percentage))
                sys.stdout.flush()

            if numadded == len(taxid_dict):  # if we have all names, we don't need to go further
                break

    sys.stdout.write('Read {} names / Added {} (100.00%) names\n'.format(numread, numadded))
    sys.stdout.flush()

    return taxid_dict


def clean_ncbi_sequence_name(ncbi_sequence_name):
    """
    Function that cleans up sequence descriptions as found in the NCBI sequence annotations by keeping only printable
    ASCII characters (everything else gets replaced by an underscore) as well as squeezing consecutive underscores
    together and removing leading and trailing underscores.

    :param ncbi_sequence_name: str sequence description as found in NCBI fastas
    :return: str cleaned up name
    """

    cleanname_list = []
    for character in ncbi_sequence_name.strip():
        if character in keepchars:
            cleanname_list.append(character)
        else:
            cleanname_list.append('_')
    cleanname = ''.join(cleanname_list)  # put everything back together as a string
    # squeeze several consecutive underscores into one and strip leading and trailing underscores
    return '_'.join([x for x in cleanname.split('_') if x])


def build_accession_number_to_taxid_dictionary(acc_to_ti_gz_file_path, acc_to_ti_dict=None):
    """
    Function that builds a accession number to taxid dictionary by parsing the given acc_to_ti file.
    If an already existing dictionary is given as the acc_to_ti_dict optional argument it will fill up this dictionary
    with matching entries ONLY and therefore possibly save lots of RAM.

    :param acc_to_ti_gz_file_path: str path to a gz compressed NCBI accession number to taxid mapping file
    :param acc_to_ti_dict: dict as generated by gather_accession_numbers_from_fasta_file()
    :return: return dict {str accession_number: int tax_id}
    """

    dict_given = True
    if not acc_to_ti_dict:  # if we have no pre-built dictionary, we initialize one now
        acc_to_ti_dict = {}
        dict_given = False

    no_of_accs_needed = len(acc_to_ti_dict)  # remember how many mappings we need to read
    accs_set = 0
    lines_read = 0

    with gzip.open(acc_to_ti_gz_file_path, 'rt') as acc_to_ti_file:
        for line in acc_to_ti_file:
            line_split = [x.strip() for x in line.split()]
            acc_plus_ver = line_split[0]
            taxid = int(line_split[1])
            lines_read += 1

            if acc_plus_ver in acc_to_ti_dict or not dict_given:  # add to dict if previously seen or no dict given
                acc_to_ti_dict[acc_plus_ver] = taxid
                accs_set += 1

            if lines_read % 1000 == 0:
                percentage = float(accs_set)/no_of_accs_needed
                sys.stdout.write('Read {} accession to taxid mappings, added {} ({:.2%})\r'
                                 .format(lines_read, accs_set, percentage))
                sys.stdout.flush()

            if dict_given and accs_set == no_of_accs_needed:  # don't look any further if we have all needed accs
                break

    percentage = float(accs_set) / no_of_accs_needed
    sys.stdout.write('Read {} accession to taxid mappings, added {} ({:.2%})\n'.format(
        lines_read, accs_set, percentage))
    sys.stdout.flush()

    return acc_to_ti_dict


def single_ncbi_annotation_to_our_defline(defline, taxid_dict, acc_to_taxid_dict, chunk_separator='|',
                                          info_separator=':'):
    """
    Function that creates our defline for a given single NCBI sequence annotation.

    :param defline: str NCBI sequence annotation defline
    :param taxid_dict: dict {int node_ti: [int node_parent_ti, str node_name, str node_rank]}
    :param acc_to_taxid_dict: dict {str accession_number: int ti}
    :param chunk_separator: str to be used as separator between such chunks
    :param info_separator: str to be used as separator between the info in this chunk
    :return: str defline in our format '<acc>|<rank>:<ti>:<name>|...|no_rank:1:root'
    """

    root = defline_chunk_for_one_tax_node()  # this function gives root node without parameters

    # get acc from name
    acc = defline[0]

    # try to get initial taxid by querrying the acc_to_taxid_dict, if not possible assign to root
    try:
        nodeid = acc_to_taxid_dict[acc]
    except KeyError:
        return "{}{}".format(acc, root)

    # new name from acc and full path
    return ''.join([acc, create_full_taxonomic_defline_from_node(nodeid, taxid_dict, chunk_separator, info_separator)])


def create_full_taxonomic_defline_from_node(start_taxid, taxid_dict, chunk_separator='|', info_separator=':'):
    """
    Function that gives the taxonomic part of our defline without the leading accession number(s).

    :param start_taxid: int id of the lowest taxonomic node in the path
    :param taxid_dict: TaxID dictionary
    :param chunk_separator: str to be used as separator between such chunks
    :param info_separator: str to be used as separator between the info in this chunk
    :return: str taxonomic part of our defline format
    """

    nodeid = start_taxid
    full_path = []

    while nodeid != 1:  # we traverse through all nodes until we reach root
        if nodeid in taxid_dict:
            # add current level
            full_path.append(defline_chunk_for_one_tax_node(
                taxid_dict[nodeid][2], nodeid, taxid_dict[nodeid][1], chunk_separator, info_separator))

            # set next level / parent
            nodeid = taxid_dict[nodeid][0]

        else:  # break if we don't find some node
            break

    # inner function gives root node
    full_path.append(defline_chunk_for_one_tax_node(chunk_separator=chunk_separator, info_separator=info_separator))

    return ''.join(full_path)


def defline_chunk_for_one_tax_node(rank='no_rank', taxid=1, name='root', chunk_separator='|', info_separator=':'):
    """
    Function that generates a defline chunk in the format '<rank>:<ti>:<name>' if the default separators are used.
    With all defaults generates the chunk for the root node in the taxonomic tree.

    :param rank: str taxonomic rank of the node in the taxonomic tree
    :param taxid: int taxonomic id of the node in the taxonomic tree
    :param name: str taxonomic name of the node in the taxonomic tree
    :param chunk_separator: str to be used as separator between such chunks
    :param info_separator: str to be used as separator between the info in this chunk
    :return: str defline chunk constructed from the given info in the format '<rank>:<ti>:<name>'
    """

    return ''.join([chunk_separator, rank, info_separator, str(taxid), info_separator, name])


def find_common_path_in_our_deflines(list_of_deflines, chunk_separator='|'):
    """
    Function that determines the common taxonomic path of all given deflines of our format conserving all accession
    numbers of the given deflines.

    :param list_of_deflines: list [str defline]
    :param chunk_separator: str character to split the defline by during the search
    :return: str defline designating the common taxonomic path
    """

    if len(list_of_deflines) == 1:  # only one defline in list -> that's the common path then
        return list_of_deflines[0]

    sorted_acc_list = sorted([name.split('|')[0] for name in list_of_deflines])
    joined_accs = ",".join(sorted_acc_list)

    # split all deflines by chunk separator and reverse them -> matrix of chunks
    rev_chunk_mat = [x.split(chunk_separator)[::-1] for x in list_of_deflines]

    # go through matrix column wise and keep the common columns
    new_defline_chunks = []
    for index, item in enumerate(rev_chunk_mat[0]):  # iterate over the first defline and see if all others are the same
        if all([x[index] == rev_chunk_mat[0][index] for x in rev_chunk_mat[1:]]):
            new_defline_chunks.append(rev_chunk_mat[0][index])
        else:
            break

    # return new common defline with joined accession numbers at the front by reversing and joining
    new_defline_chunks.append(joined_accs)
    return chunk_separator.join(new_defline_chunks[::-1])


def count_chunks_in_defline(defline, chunk_separator='|'):
    """
    Function that counts the chunks in a given defline for example to detect if the defline contains only the root
    annotation (count == 1).

    :param defline: str defline of our format (could also use NCBI defline if we need to count the whole annotations)
    :param chunk_separator: str character to count
    :return: int count of the found chunk_separator
    """

    count = 0
    for i in range(len(defline)):
        if defline[i:i+len(chunk_separator)] == chunk_separator:
            count += 1

    return count  # since we have one separator for each chunk


def write_to_fasta_file(sequence_tuple, fasta_file_handle):
    """
    Function that writes a given sequence (str name, str bases) to a fasta file unwrapped.

    :param sequence_tuple: list of tuple (str name, str bases)
    :param fasta_file_handle: writable file object for the output
    :return: None
    """

    fasta_file_handle.write('>')
    fasta_file_handle.write(sequence_tuple[0])
    fasta_file_handle.write('\n')
    fasta_file_handle.write(sequence_tuple[1])
    fasta_file_handle.write('\n')


def process_input_file(input_fasta_gz_file, output_fasta_gz_file, taxid_dict, acc_to_ti_dict, conserve_ncbi_names=True,
                       skip_root_only_sequences=True, skip_n_only_sequences=True, chunk_separator='|',
                       annotation_separator='||', info_separator=':', compression_level=6):
    """
    Function that processes an input fasta file with NCBI sequence annotations to our format, either keeping the
    NCBI annotations or discarding them.

    :param input_fasta_gz_file: str path to the gz compressed input fasta file
    :param output_fasta_gz_file: str path to the gz compressed output fasta file
    :param taxid_dict: TaxID dictionary {int node_id: [int node_parent_id, str node_name, str node_rank]}
    :param acc_to_ti_dict: Accession number to TaxID mapping {str accession_number: int tax_id}
    :param conserve_ncbi_names: bool designating whether to keep the original NCBI annotations
    :param skip_root_only_sequences: bool deciding whether to keep sequences with taxonomic annotation to only root
    :param skip_n_only_sequences: bool designating whether to keep sequences without real information
    :param chunk_separator: str used to join defline chunks
    :param annotation_separator: str used to join different annotations (bigger units than chunks)
    :param info_separator: str to be used as separator between the info in taxinfo chunks, e.g. between rank and taxid
    :param compression_level: int between 1 and 9 designating the gzip compression level to use
    :return: str output file path
    """

    sys.stdout.write('Reading {} / writing {} ...\n'.format(input_fasta_gz_file, output_fasta_gz_file))
    gen = fasta_gz_file_read_generator(input_fasta_gz_file)
    seqnumread = 0
    seqnumwritten = 0

    with gzip.open(output_fasta_gz_file, 'wt', compression_level) as outfile:
        for read_name, read_sequence in gen:
            seqnumread += 1

            # check if sequence only contains Ns
            if read_sequence.strip('N') == '' and skip_n_only_sequences:
                continue

            # split annotations and clean names
            single_annotations = split_defline(read_name)

            # calculate our defline for each annotation
            our_defline_list = []
            for single_annotation in single_annotations:
                our_defline_list.append(
                    single_ncbi_annotation_to_our_defline(
                        single_annotation, taxid_dict, acc_to_ti_dict, chunk_separator, info_separator))

            # find common path in all our deflines
            our_new_defline = find_common_path_in_our_deflines(our_defline_list, chunk_separator)

            # skip root only sequences if desired
            if count_chunks_in_defline(our_new_defline, chunk_separator) == 1 and skip_root_only_sequences:
                continue

            # construct new defline including converted NCBI and our annotation
            if conserve_ncbi_names:
                single_annotations = [chunk_separator.join(x) for x in single_annotations] # merging tuples
                single_annotations.append(our_new_defline)
                new_read_name = annotation_separator.join(single_annotations)
            else:  # if we don't conserve the ncbi deflines we just take our new defline as sequence annotation
                new_read_name = our_new_defline

            # write the new sequence to the output fasta file
            write_to_fasta_file((new_read_name, read_sequence), outfile)
            seqnumwritten += 1

            # some verbosity
            if seqnumread % 1000 == 0:
                sys.stdout.write('Read {} / wrote {} sequences\r'.format(seqnumread, seqnumwritten))
                sys.stdout.flush()

    sys.stdout.write('Read {} / wrote {} sequences\n'.format(seqnumread, seqnumwritten))
    sys.stdout.flush()

    return output_fasta_gz_file


def convert_fasta_gz_file_to_our_format(input_fasta_gz_file, output_fasta_gz_file, nodes, names, acc_to_ti_gz,
                                        conserve_ncbi_names=True, skip_root_only_sequences=True,
                                        skip_n_only_sequences=True, chunk_separator='|', annotation_separator='||',
                                        info_separator=':', compression_level=6):
    """
    Function that converts fasta files with the standard NCBI format to our format, changing the sequence names
    to the whole taxonomic path for every entry.

    :param input_fasta_gz_file: str path to gz compressed input fasta file
    :param output_fasta_gz_file: str path to gz compressed output fasta file
    :param nodes: str path to nodes.dmp
    :param names: str path to names.dmp
    :param acc_to_ti_gz: str path to acc_to_taxid.txt.gz
    :param conserve_ncbi_names: bool designating whether to keep the original NCBI annotations
    :param skip_root_only_sequences: bool deciding whether to keep sequences with taxonomic annotation to only root
    :param skip_n_only_sequences: bool designating whether to keep sequences without real information
    :param chunk_separator: str used to join defline chunks
    :param annotation_separator: str used to join different annotations (bigger units than chunks)
    :param info_separator: str to be used as separator between the info in a chunk, e.g. between rank and taxid
    :param compression_level: int between 1 and 9 designating the gzip compression to use
    :return: str path to output_file
    """

    taxid_dict = build_taxid_dictionary(nodes)
    taxid_dict = add_names_to_taxid_dictionary(names, taxid_dict)
    acc_to_ti_dict = gather_accession_numbers_from_fasta_gz_file(input_fasta_gz_file)
    acc_to_ti_dict = build_accession_number_to_taxid_dictionary(acc_to_ti_gz, acc_to_ti_dict)
    return process_input_file(input_fasta_gz_file, output_fasta_gz_file, taxid_dict, acc_to_ti_dict,
                              conserve_ncbi_names, skip_root_only_sequences, skip_n_only_sequences, chunk_separator,
                              annotation_separator, info_separator, compression_level)


def run_subprocess_with_returncode(command, capture_streams=False):
    """
    Function that launches a subprocess, waits for it to terminate and returns the returncode and if desired the output.

    :param command: str shell command to execute
    :param capture_streams: bool designating whether
    :return: tuple of (int returncode of the executed program, tuple of (stdout, stderr))
    """

    stdout = b''
    stderr = b''

    if capture_streams:
        proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
    else:
        proc = subprocess.Popen(command, shell=True)

    returncode = proc.wait()
    streams = (stdout.decode('ascii', 'replace'), stderr.decode('ascii', 'replace'))

    return returncode, streams


def download_file(file_url, target_directory, dependencies, max_tries=100, http_proxy='', https_proxy='', ftp_proxy=''):
    """
    Function that downloads a file from an online location to a local destination using the command line tool wget.
    If a directory URL (ending in /) is given, it lets wget do the interpretation of the URL, but saves the file named
    after the directory.

    :param file_url: str URL to the remote file
    :param target_directory: str path to the local file destination
    :param dependencies: dict {str program: str path}
    :param max_tries: int number designating the number of times to try the download before raising an exception
    :param http_proxy: str URL to the HTTP proxy to use
    :param https_proxy: str URL to the HTTPS proxy to use
    :param ftp_proxy: str URL to the FTP proxy to use
    :return: str path to the downloaded file
    """

    os.makedirs(target_directory, exist_ok=True)

    basename = os.path.basename(file_url)
    if not basename:  # supposedly the file_url ended in /
        basename = os.path.basename(file_url[:-1])

    file_path = os.path.join(target_directory, basename)
    returncode = 1
    tries = 0

    if http_proxy:
        os.environ['http_proxy'] = http_proxy
    if https_proxy:
        os.environ['https_proxy'] = https_proxy
    if ftp_proxy:
        os.environ['ftp_proxy'] = ftp_proxy

    while returncode:
        tries += 1
        returncode, streams = run_subprocess_with_returncode('{} -O {} {}'.format(
            dependencies['wget'], file_path, file_url), True)
        if tries == max_tries:
            raise FailedToExecuteException(streams[1])
        if returncode:
            time.sleep(3)  # hard coded wait time to try to compensate for short proxy unavailabilities

    return file_path


def verify_file_integrity(file_path, md5_path, dependencies, delete_md5_file_if_check_succeeded=True):
    """
    Function that utilizes md5sum to check the integrity of a given file with a given md5 file.

    :param file_path: str path to the file to check
    :param md5_path: str path to the md5 file to compare with
    :param dependencies: dict {str program: str path}
    :param delete_md5_file_if_check_succeeded: bool designating whether to remove the md5 file after successful check
    :return: bool designating the success of the md5 check
    """

    returncode, streams = run_subprocess_with_returncode('{} {} | {} -d " " -f 1'.format(
        dependencies['md5sum'], file_path, dependencies['cut']), True)

    md5_from_file = streams[0].rstrip()
    md5_from_md5file = open(md5_path).read().split()[0]

    if md5_from_file == md5_from_md5file:
        if delete_md5_file_if_check_succeeded:
            os.remove(md5_path)
        return True
    else:
        return False


def download_and_verify_file(file_url, md5_url, target_directory, dependencies, max_tries=10, http_proxy='',
                             https_proxy='', ftp_proxy=''):
    """
    Function that downloads a file and a corresponding md5 file and checks the integrity after the download.

    :param file_url: str URL to the file to download
    :param md5_url: str URL to the md5 file corresponding to the file to download
    :param target_directory: str path to the directory to download to
    :param dependencies: dict {str program: str path}
    :param max_tries: int number designating the number of times to try the download before raising an exception
    :param http_proxy: str URL to the HTTP proxy to use
    :param https_proxy: str URL to the HTTPS proxy to use
    :param ftp_proxy: str URL to the FTP proxy to use
    :return: str path to the successfully downloaded and verified file
    """

    file_path = ''
    unverified = True

    while unverified:
        md5_path = download_file(md5_url, target_directory, dependencies, max_tries, http_proxy, https_proxy, ftp_proxy)
        file_path = download_file(file_url, target_directory, dependencies, max_tries, http_proxy, https_proxy,
                                  ftp_proxy)
        unverified = not verify_file_integrity(file_path, md5_path, dependencies)

    return file_path


def unpack_tar_gz_file(tar_gz_file_path, target_directory, dependencies, threads=1, delete_file_after_extract=True):
    """
    Function that wraps both unpacking functions.

    :param tar_gz_file_path: str path to the tar.gz file
    :param target_directory: str path to the directory into which to extract
    :param dependencies: dict {str program: str path}
    :param threads: number of threads to use for the gzip decompression
    :param delete_file_after_extract: bool designating whether to delete the source file after extraction
    :return: List of str paths to all extracted files
    """

    old_wd = os.getcwd()
    os.chdir(target_directory)

    returncode, streams = run_subprocess_with_returncode('{} -dfckp {} {} | {} --overwrite -xvf -'.format(
        dependencies['pigz'], threads, tar_gz_file_path, dependencies['tar']), True)
    if returncode != 0:
        raise FailedToExecuteException(streams[1])
    res = [os.path.join(target_directory, x) for x in streams[0].split('\n')]

    if delete_file_after_extract:
        os.remove(tar_gz_file_path)
    os.chdir(old_wd)

    # return all extracted files (but no dirs) in a list
    return [x for x in res if os.path.isfile(x)]


def unpack_gz_file(gz_file_path, target_directory, dependencies, threads=1, delete_file_after_extract=True):
    """
    Function that unpacks a gz file using the command line tool pigz.

    :param gz_file_path: str path to a gz file to unpack
    :param target_directory: str path to the directory to extract to
    :param dependencies: dict {str program: str path}
    :param threads: int number of threads to utilize
    :param delete_file_after_extract: bool designating whether to delete the source file after extraction
    :return: str path of the unpacked file
    """

    outfile_name = os.path.join(target_directory, os.path.splitext(os.path.basename(gz_file_path))[0])

    returncode, streams = run_subprocess_with_returncode('{} -dfckp {} {} > {}'.format(
        dependencies['pigz'], threads, gz_file_path, outfile_name), True)

    if returncode:
        raise FailedToExecuteException(streams[1])

    if delete_file_after_extract:
        os.remove(gz_file_path)

    return outfile_name  # simply returning the input path without the .gz extension which should be our new file


def set_subdirectory_to_work_in(directory):
    """
    Function that creates and returns the path to a new integer named subdirectory in the given directory.

    :param directory: str path to the base directory
    :return: tuple: (str path to the newly created subdirectory, list [str path to previously existing int directories])
    """

    os.makedirs(directory, exist_ok=True)

    int_subdirs = get_integer_named_subdirectories(directory)
    try:
        new_subdir = str(max(int_subdirs)+1)  # highest previous integer named dir + 1
    except ValueError:  # no previous subdir ... make a subdir called 1
        new_subdir = '1'
    new_path = os.path.join(directory, new_subdir)
    os.mkdir(new_path)
    return new_path, [os.path.join(directory, str(x)) for x in int_subdirs]


def get_integer_named_subdirectories(directory):
    """
    Function that collects and returns a list of all integer named subdirectories in the given directory.

    :param directory: str path to a directory
    :return: List of integers representing integer named subdirectories in the given directory
    """

    list_of_items_in_dir = os.listdir(directory)
    subdirs = [x for x in list_of_items_in_dir if os.path.isdir(os.path.join(directory, x))]

    int_subs = []

    for subdir in subdirs:
        try:
            int_subs.append(int(subdir))
        except ValueError:
            pass

    return int_subs


def download_and_verify_file_pattern_from_ftp_directory_url(url, pattern, target_directory, dependencies, max_tries=10,
                                                            http_proxy='', https_proxy='', ftp_proxy=''):
    """
    Function that downloads and verifies files from a given ftp directory url if they match a given pattern, defined
    by a given function.

    :param url: str FTP directory URL
    :param pattern: function returning true or false used to test the files names in the FTP directory
    :param target_directory: str path to a local directory to which to download the files
    :param dependencies: dict {str program: str path}
    :param max_tries: int number designating the number of times to try the download before raising an exception
    :param http_proxy: str URL to the HTTP proxy to use
    :param https_proxy: str URL to the HTTPS proxy to use
    :param ftp_proxy: str URL to the FTP proxy to use
    :return: List of str paths to the downloaded files
    """

    files = get_file_names_from_ncbi_ftp_directory_url(url, dependencies)

    real_files = sorted([os.path.join(url, x) for x in files if pattern(x) and not x.endswith('.md5')])
    check_files = sorted([os.path.join(url, x) for x in files if pattern(x) and x.endswith('.md5')])
    file_paths = []
    for real_file, check_file in zip(real_files, check_files):
        file_paths.append(download_and_verify_file(real_file, check_file, target_directory, dependencies, max_tries,
                                                   http_proxy, https_proxy, ftp_proxy))

    return file_paths


def concatenate_files(input_file_paths, output_file_path, delete_source_files_afterwards=True):
    """
    Function that concatenates the contents of a list of given file paths to a given output file.

    :param input_file_paths: list of str paths of input files to concatenate
    :param output_file_path: str path to the output file to write to
    :param delete_source_files_afterwards: bool designating whether to delete the source files after concatenation
    :return: str path to the output file
    """

    infiles = [open(x) for x in input_file_paths]

    with open(output_file_path, 'w') as outfile:
        for infile in infiles:
            for line in infile:
                outfile.write(line)

    if delete_source_files_afterwards:
        for infile_path in input_file_paths:
            os.remove(infile_path)

    return output_file_path


def download_taxonomy_data(taxdump_tar_gz_url, target_directory, dependencies, accession2taxid_directory_url=None,
                           nucl=True, prot=True, dead=True, threads=1):
    """
    Function that downloads all taxonomic data that is necessary to build the taxonomic tree and the accession number
    to taxonomic id mappings for the chosen databases. The download of the acc2ti mapping is optional and only performed
    if a url for it is given. This information can be extracted from a blast database as well and doesn't have to be
    downloaded twice.

    :param taxdump_tar_gz_url: str URL to the taxdump.dmp file on the NCBI servers
    :param target_directory: str path to a target directory to work in
    :param dependencies: dict {str program: str path}
    :param accession2taxid_directory_url: str URL to the accession2taxid directory on the NCBI servers
    :param nucl: bool designating whether to download the acc2ti maps for nucleic acid sequences
    :param prot: bool designating whether to download the acc2ti maps for protein sequences
    :param dead: bool designating whether to download the acc2ti maps for dead/deprecated sequences
    :param threads: int designating how many threads to use for threadable operations
    :return: tuple (str path to nodes.dmp, str path to names.dmp, str path to acc2ti.txt)
    """

    acc2ti_path = ''
    if accession2taxid_directory_url:
        # set the search pattern according to required taxmaps
        if nucl and prot and dead:
            def acc2ti_pattern(x):
                return 'README' not in x
        elif nucl and prot:
            def acc2ti_pattern(x):
                return 'README' not in x and 'dead' not in x
        elif nucl and dead:
            def acc2ti_pattern(x):
                return 'README' not in x and 'nucl' in x
        elif prot and dead:
            def acc2ti_pattern(x):
                return 'README' not in x and 'prot' in x
        elif nucl:
            def acc2ti_pattern(x):
                return 'README' not in x and 'nucl' in x and 'dead' not in x
        elif prot:
            def acc2ti_pattern(x):
                return 'README' not in x and 'prot' in x and 'dead' not in x
        else:  # raising Exception if neither nucl nor prot
            raise ValueError('At least one of the nucl and prot parameters must be true!')

        acc2ti_list = download_and_verify_file_pattern_from_ftp_directory_url(accession2taxid_directory_url,
                                                                              acc2ti_pattern, target_directory,
                                                                              dependencies)

        all_acc2ti_files = []
        # unpack accession2taxid files
        for acc2ti_gz in acc2ti_list:
            all_acc2ti_files.append(unpack_gz_file(acc2ti_gz, target_directory, dependencies))

        # concatenate accession2taxid files
        acc2ti_path = concatenate_files(all_acc2ti_files, os.path.join(target_directory, 'acc2ti.txt'))

    taxdump = download_and_verify_file(taxdump_tar_gz_url, taxdump_tar_gz_url+'.md5', target_directory, dependencies)

    # unpack taxdump stuff
    taxdump_files = unpack_tar_gz_file(taxdump, target_directory, dependencies, threads)
    nodes_path = [x for x in taxdump_files if x.endswith('nodes.dmp') and not x.endswith('delnodes.dmp')][0]
    names_path = [x for x in taxdump_files if x.endswith('names.dmp')][0]

    # remove unnecessary taxdump files
    other_files = [x for x in taxdump_files if not x.endswith('nodes.dmp') and not x.endswith('names.dmp') or
                   x.endswith('delnodes.dmp')]
    for other_file in other_files:
        os.remove(other_file)

    # return nodes.dmp path, names.dmp path and concatenated accession2taxid file path
    return nodes_path, names_path, acc2ti_path


def extract_acc2ti_gz_from_blast_db(db_path_basename, output_file_path, dependencies, threads=1, compression_level=6,
                                    divisions=None):
    """
    Function that extracts an accession number to TaxID map from a blast database and writes it to a file.

    :param db_path_basename: str basename of the database to extract from
    :param output_file_path: str path of the gz compressed file to write to
    :param dependencies: dict {str program: str path}
    :param threads: int number of threads to use for the compression
    :param compression_level: int between 1 and 9 designating the gzip compression level to use
    :param divisions: list [str division] with divisions to include in the acc2ti map
    :return: str path of the output file
    """

    if divisions:
        grep_command = '^' + '\|^'.join(divisions)
        call = '{} -db {} -entry all -outfmt "%a %T" | {} "{}" | {} -ckp {} -{} > {}'.format(
            dependencies['blastdbcmd'], db_path_basename, dependencies['grep'], grep_command, dependencies['pigz'],
            threads, compression_level, output_file_path)
    else:
        call = '{} -db {} -entry all -outfmt "%a %T" | {} -ckp {} -{} > {}'.format(
            dependencies['blastdbcmd'], db_path_basename, dependencies['pigz'], threads, compression_level,
            output_file_path)

    returncode, streams = run_subprocess_with_returncode(call, True)

    if returncode:
        raise FailedToExecuteException(streams[1])
    return output_file_path


def extract_fasta_gz_from_blast_db(db_path_basename, output_fasta_gz_file_path, dependencies,
                                   threads=1, compression_level=6, regex_filter=''):
    """
    Function that extracts a fasta file from a blast database, optionally keeping only sequences from the list of given
    divisions.

    :param db_path_basename: str basename of the blast database
    :param output_fasta_gz_file_path: str path to the fasta.gz file to be created
    :param dependencies: dict {str program: str path}
    :param threads: int number of threads to use for the compression
    :param compression_level: int between 1 and 9 designating the gzip compression to use
    :param regex_filter: str perl style regular expression identifying entries which to keep
    :return: str path to the output fasta.gz file
    """

    if regex_filter:
        call = '{0} -db {1} -entry all | {2} \'/^>/ {{ print (NR==1 ? "" : RS) $0; next }} {{ printf "%s", $0 }} END ' \
               '{{ printf RS }}\' | {3} -iP -A 1 \'{4}\' | {3} -v "^--$" | {5} -ckp {6} -{7} > {8}'.format(
                dependencies['blastdbcmd'], db_path_basename, dependencies['awk'], dependencies['grep'], regex_filter,
                dependencies['pigz'], threads, compression_level, output_fasta_gz_file_path)
    else:
        call = '{0} -db {1} -entry all | {2} \'/^>/ {{ print (NR==1 ? "" : RS) $0; next }} {{ printf "%s", $0 }} END ' \
               '{{ printf RS }}\' | {3} -ckp {4} -{5} > {6}'.format(
                dependencies['blastdbcmd'], db_path_basename, dependencies['awk'], dependencies['pigz'], threads,
                compression_level, output_fasta_gz_file_path)

    returncode, streams = run_subprocess_with_returncode(call, True)

    if returncode:
        raise FailedToExecuteException(streams[1])

    return output_fasta_gz_file_path


def split_our_fasta_gz_into_subsets(fasta_gz_file_path, taxonomies, info_separator=':', compression_level=6):
    """
    Function that splits a fasta file in our annotation format into different fasta files based on the taxonomy of each
    entry and a given list of desired taxonomies to generate a subset fasta for.

    :param fasta_gz_file_path: str path to a fasta file in our annotation format
    :param taxonomies: list [list [str name, int TaxID]]
    :param info_separator: str to be used as separator between the info in taxinfo chunks, e.g. between rank and taxid
    :param compression_level: int between 1 and 9 designating the gzip compression level to use
    :return: list [str paths_to_newly_created_fasta_files]
    """

    # get base path for all databases (which is .. to the database path)
    working_dir = os.path.dirname(fasta_gz_file_path)

    # create folders for each taxonomy
    for taxonomy in taxonomies:
        os.makedirs(os.path.join(working_dir, taxonomy[0]), exist_ok=True)

    # create file handles for all taxonomies [taxid of the taxonomy desired, corresponding file handle]
    # adding the info separator around the taxid so we don't find it somewhere in a bigger number
    file_handles = [[info_separator+str(x[1])+info_separator,
                     gzip.open(os.path.join(working_dir, x[0], x[0]+".fa.gz"), 'wt', compression_level)]
                    for x in taxonomies]

    # go through the input file read wise and write to all corresponding taxonomy files
    gen = fasta_gz_file_read_generator(fasta_gz_file_path)

    for read in gen:
        for file_handle in file_handles:
            if file_handle[0] in read[0]:
                write_fasta_sequence(file_handle[1], read)

    # return list of paths to the newly created fasta files
    return [x[1].name for x in file_handles]


def write_fasta_sequence(file_handler, seq):
    """
    Short wrapper function for writing sequences in fasta format to a file.

    :param file_handler: File handle object for a file to write to
    :param seq: list [str name (without leading >), str sequence]
    :return: None
    """

    file_handler.write(">")
    file_handler.write(seq[0])
    file_handler.write("\n")
    file_handler.write(seq[1])
    file_handler.write("\n")


def extend_file_name(name, extend_with, separator="_"):
    """
    Extends a file name by adding a String between name and extension, transforming file.ext to file_addition.ext

    :param name: String path to the file
    :param extend_with: String with with to extend the file name
    :param separator: String to be used as separator of old name and extension
    :return: String path of the new file name
    """

    inputfile_ext_split = os.path.splitext(name)
    return ''.join([inputfile_ext_split[0], separator, str(extend_with), inputfile_ext_split[1]])


def build_indices_threaded(references, indexing_function, dependencies, threads=1):
    """
    Function that parallelizes building of several mapper indices

    :param references: list [str path to fasta.gz file]
    :param indexing_function: function to use to build indices out of the given fasta files
    :param dependencies: dict {str program: str path}
    :param threads: int designating how many threads to use for the index building
    :return: list [str basenames of the newly build indices]
    """

    with multiprocessing.Pool(threads) as pool:
        # don't collect Nones from the indexing func
        return [x for x in pool.starmap(indexing_function, zip(references, itertools.repeat(dependencies))) if x]


def build_bowtie2_index(reference, dependencies, threads=1):
    """
    A function that builds a bowtie2 index for the given fasta file in the index subdirectory

    :param reference: str path to fasta.gz file
    :param dependencies: dict {str program: str path}
    :param threads: int designating how many threads should be used for indexing
    :return: str designating the basename of the files created
    """

    outputdir = os.path.join(os.path.dirname(reference), "bowtie2")

    os.makedirs(outputdir, exist_ok=True)
    shortref = os.path.splitext(os.path.splitext(os.path.basename(reference))[0])[0]
    basename = os.path.join(outputdir, shortref)
    # since bowtie2 can neither build from gz compressed fastas nor from stdin, we extract the fasta, build and remove
    fasta_path = unpack_gz_file(reference, os.path.dirname(reference), dependencies, threads, False)
    call = '{} --large-index --threads {} {} {}'.format(dependencies['bowtie2-build'], threads, fasta_path, basename)
    sys.stdout.write("Building bowtie2 index with {}\n".format(call))
    returncode, streams = run_subprocess_with_returncode(call, True)
    os.remove(fasta_path)
    if returncode:
        if 'Empty fasta file' in streams[1]:  # silently ignore empty fastas
            return None
        else:
            raise FailedToExecuteException(streams[1])

    return basename


def build_blast_index(reference, dependencies):
    """
    A function that builds a blast database for the given fasta file in the blast subdirectory

    :param reference: String path to fasta.gz file
    :param dependencies: dict {str program: str path}
    :return: String designating the basename of the files created
    """

    outputdir = os.path.join(os.path.dirname(reference), "blast")

    os.makedirs(outputdir, exist_ok=True)
    shortref = os.path.splitext(os.path.splitext(os.path.basename(reference))[0])[0]  # double split because .fa.gz
    basename = os.path.join(outputdir, shortref)
    call = '{} {} | {} -dbtype nucl -title {} -out {}'.format(
        dependencies['zcat'], reference, dependencies['makeblastdb'], shortref, basename)
    sys.stdout.write("Building BLAST index with {}\n".format(call))
    returncode, streams = run_subprocess_with_returncode(call, True)
    if returncode:
        # we silently ignore empty fasta files to build indices for
        if 'is empty' in streams[1] or 'No sequences added' in streams[1]:
            return None
        else:
            raise FailedToExecuteException(streams[1])

    return basename


def merge_databases_redundancy_free(list_of_fasta_gzs, output_fasta_gz, compression_level=6):
    """
    A function that merges fasta files redundancy free by checking whether a read of the same name has already been
    added before.

    :param list_of_fasta_gzs: list [str paths_to_fasta_gz_file]
    :param output_fasta_gz: str path_to_output_fasta_gz_file
    :param compression_level: int gzip compression level to use
    :return: str path to the newly created output fasta.gz file
    """

    seqs = set()
    with gzip.open(output_fasta_gz, mode='wt', compresslevel=compression_level) as outfile:
        for fasta_gz in list_of_fasta_gzs:
            gen = fasta_gz_file_read_generator(fasta_gz)
            for read in gen:
                if read[0] in seqs:  # if we already added a sequence with that name ... we don't add it again
                    sys.stdout.write('Ignoring {} as a redundant entry\n'.format(read[0]))
                    continue
                else:
                    seqs.add(read[0])
                    write_fasta_sequence(outfile, read)

    return output_fasta_gz


def merge_acc2ti_files_redundancy_free(list_of_acc2ti_gzs, output_acc2ti_gz, compression_level=6):
    """
    A function that merges acc2ti.gz files redundancy free by checking whether an accession of the same name has already
    been added before.

    :param list_of_acc2ti_gzs: list [str paths_to_acc2ti_gz_file]
    :param output_acc2ti_gz: str path_to_output_acc2ti_gz_file
    :param compression_level: int gzip compression level to use
    :return: str path to the newly created output acc2ti.gz file
    """

    accs = set()
    with gzip.open(output_acc2ti_gz, mode='wt', compresslevel=compression_level) as outfile:
        for acc2ti_gz in list_of_acc2ti_gzs:
            with gzip.open(acc2ti_gz, 'rt') as infile:
                for line in infile:
                    line_split = line.rstrip().split()
                    if line_split[0] in accs:
                        sys.stdout.write('Ignoring {} as a redundant entry\n'.format(line_split[0]))
                        continue
                    else:
                        accs.add(line[0])
                        outfile.write(line)

    return output_acc2ti_gz


def main():
    """
    Main function of the downloader that basically automates all the steps using the information given in the
    accompanying configuration file.

    :return: None
    """

    # startup checks and setup
    logger = setup_logger(config.logging_level, 'DB_UP')
    logger.debug('Checking for other running instances')
    check_if_script_is_singleton()
    logger.debug('Checking dependencies')
    dependencies = check_dependencies(config.dependencies)

    # setting sub directory
    logger.debug('Setting up working directories')
    work_dir, previous_dirs = set_subdirectory_to_work_in(config.working_directory)

    # for every database given in the config
    logger.debug('Processing databases')
    fasta_gz_paths = []
    acc2ti_gz_paths = []
    basenames = []
    acc2ti_path = ''
    for database_name, database_regex_filter in sorted(config.nucl_blast_databases.items()):

        # download db
        logger.debug('Downloading {}'.format(database_name))
        def pattern(x):
            return x.startswith(database_name)
        tar_gz_files = download_and_verify_file_pattern_from_ftp_directory_url(
            blast_db_folder_url, pattern, work_dir, dependencies, max_tries=10, http_proxy=config.http_proxy,
            https_proxy=config.https_proxy, ftp_proxy=config.ftp_proxy)

        # extract data
        logger.debug('Extracting files')
        unpacked_files = []
        for tar_gz_file in tar_gz_files:
            unpacked_files.extend(unpack_tar_gz_file(tar_gz_file, work_dir, dependencies, threads=config.other_threads))

        # extract acc2ti data from db
        logger.debug('Extracting accession to taxid mappings')
        db_basepath = os.path.join(work_dir, database_name)
        basenames.append(database_name)
        acc2ti_path = extract_acc2ti_gz_from_blast_db(db_basepath, db_basepath + "_acc2ti.txt.gz", dependencies,
                                                      config.other_threads)
        acc2ti_gz_paths.append(acc2ti_path)

        # extract desired divisions / text filter hits in fasta.gz format
        logger.debug('Extracting desired divisions')
        fasta_gz_paths.append(extract_fasta_gz_from_blast_db(db_basepath, db_basepath + ".fa.gz",
                                                             dependencies, config.other_threads,
                                                             config.compression_level, database_regex_filter))

        # delete blast db after extraction
        logger.debug('Deleting unextracted blast db files')
        for unpacked_file in unpacked_files:
            try:
                os.remove(unpacked_file)
            except FileNotFoundError:  # nal files will be in the list more than once
                pass

    # merge databases and acc2tis if we acquired more than one
    logger.debug('Merging databases')
    if not fasta_gz_paths:
        raise ValueError('No databases given to process')
    elif len(fasta_gz_paths) == 1:
        fasta_gz_file_path = fasta_gz_paths[0]
        db_basename = basenames[0]
        db_basepath = os.path.join(work_dir, db_basename)
    else:
        db_basename = '_'.join(basenames+['merged'])
        db_basepath = os.path.join(work_dir, db_basename)
        fasta_gz_file_path = merge_databases_redundancy_free(fasta_gz_paths, db_basepath+'.fa.gz',
                                                             config.compression_level)
        acc2ti_path = merge_acc2ti_files_redundancy_free(acc2ti_gz_paths, db_basepath+'_acc2ti.txt.gz',
                                                         config.compression_level)
        for fa_gz in fasta_gz_paths:  # remove single dbs after merging
            os.remove(fa_gz)
        for acc2ti_gz in acc2ti_gz_paths:
            os.remove(acc2ti_gz)

    # download names and nodes / don't save acc2ti map, since we use the one we created before
    logger.debug('Downloading taxonomy data')
    nodes_path, names_path, __ = download_taxonomy_data(taxdump_url, work_dir, dependencies,
                                                        threads=config.other_threads)

    # create new dir with db name and reannotate db with our format including lots of options from the config file
    logger.debug('Setting up directory for reannotation')
    os.makedirs(db_basepath, exist_ok=True)

    # convert the database to our format
    logger.debug('Converting database annotations')
    converted_fasta_path = convert_fasta_gz_file_to_our_format(
        fasta_gz_file_path, os.path.join(db_basepath, db_basename+".fa.gz"), nodes_path, names_path, acc2ti_path,
        config.conserve_ncbi_names, config.skip_root_only_sequences, config.skip_n_only_sequences,
        config.chunk_separator, config.annotation_separator, config.info_separator, config.compression_level)

    # delete acc2ti map, original fasta, names and nodes
    logger.debug('Remove accession to taxid mappings, taxonomy data and pre-conversion fasta files')
    os.remove(names_path)
    os.remove(nodes_path)
    os.remove(acc2ti_path)
    os.remove(fasta_gz_file_path)

    # split desired taxonomies as well as parts of the whole db
    logger.debug('Splitting database')
    subtax_fasta_gzs = []
    if config.taxonomies:
        subtax_fasta_gzs = sorted(split_our_fasta_gz_into_subsets(converted_fasta_path, config.taxonomies,
                                                                  config.info_separator, config.compression_level))

    # build bowtie2 indices for the subtax databases
    logger.debug('Building bowtie2 indices')
    for subtax_fasta_gz in subtax_fasta_gzs:
        logger.debug('Building {}'.format(subtax_fasta_gz))
        build_bowtie2_index(subtax_fasta_gz, dependencies, config.indexing_threads)

    # build blast indices for the subtax databases and the parts of the whole db
    logger.debug('Building BLAST indices')
    build_indices_threaded(subtax_fasta_gzs, build_blast_index, dependencies, config.indexing_threads)

    # unlink old db / link to the new database
    logger.debug('Unlinking previous database version and linking new version')
    try:
        os.unlink(config.link_directory)
    except OSError:
        pass
    os.makedirs(os.path.dirname(config.link_directory), exist_ok=True)
    os.symlink(work_dir, config.link_directory)

    # clean up old database if desired
    if config.delete_other_database_versions:
        logger.debug('Removing old database version')
        for previous_dir in previous_dirs:
            shutil.rmtree(previous_dir, ignore_errors=True)


if __name__ == '__main__':
    main()
