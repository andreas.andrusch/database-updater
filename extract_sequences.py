#!/usr/bin/env python

"""
Purpose of this script is to extract sequences from NCBI databases that are taxonomically sorted _under_ a given taxid,
e.g. all viridae or all bacteria

This is mainly done to be able to build partial databases for some annotation tools/pipelines but can possibly find
other uses as well.
"""

import sys


class FileFormatException(Exception):
    pass


def main():
    """
    Main routine of the program.

    :return: None
    """

    check_input()

    taxid = sys.argv[1]
    database = sys.argv[2]
    nodes = sys.argv[3]
    gi_taxid = sys.argv[4]

    sys.stderr.write("Building set of Tax IDs to keep ...\n")
    taxids = build_taxid_set(taxid, nodes)
    sys.stderr.write("Translating set to GIs to keep ...\n")
    gis = translate_taxid_set_to_gi_set(taxids, gi_taxid)
    sys.stderr.write("Writing out database entries to keep ...\n")
    extract_entries_from_db(database, gis)
    sys.stderr.write("Done! :)\n")


def check_input():
    """
    Checks if the program was called correctly and closes it, printing instructions if not.

    :return: None
    """

    if len(sys.argv) != 5:
        sys.stderr.write(
        "Excecute with python extract_sequences.py <taxid> <db.fa> <nodes.dmp> <gi_taxid.dmp> > <output.fa>\n\n"
        "where <taxid> designates the node under which all entries shall be extracted,\n"
        "<db.fa> is the path to the nt or nr database downloaded from NCBI,\n"
        "<nodes.dmp> is the path to the nodes.dmp,\n"
        "<gi_taxid.dmp> is the path to gi_taxid_X.dmp, where X must correspond with the chosen database,\n"
        "<output.fa> is the path to the file the kept entries shall be written to.\n\n"
        "Entries with more than one GI number will be kept if one of them is a child of the given Tax ID.\n")
        exit(1)


def build_taxid_set(taxid, nodes):
    """
    Builds a set of Tax IDs with the parameter taxid being the parent or grand-parent etc. of all other contained ids.

    :param taxid: Taxid of which you want child nodes and their child etc.
    :param nodes: String path to nodes.dmp file
    :return: Set of Tax IDs
    """

    all_taxids = {}
    counter = 0
    taxids = set()

    with open(nodes) as infile:
        for line in infile:
            counter += 1
            line_split = line.split("|")
            child = line_split[0].strip()
            parent = line_split[1].strip()

            try:
                all_taxids[parent].append(child)
            except KeyError:
                all_taxids[parent] = [child]

            if counter%1000 == 0:
                sys.stderr.write("Read {} Tax ID mappings ...\r".format(counter))

    sys.stderr.write("Read {} Tax ID mappings ...\n".format(counter))

    add_taxids_recursively(taxid, all_taxids, taxids)
    sys.stderr.write("Saved {} Tax ID mappings.\n".format(len(taxids)))

    return taxids


def add_taxids_recursively(taxid, taxid_map_dict, set_of_taxids):
    """
    Function that recursively adds a taxid and its children to a set of taxids in place.

    :param taxid: Tax ID to add and for whose children this function will be called as well
    :param taxid_map_dict: Dictionary of Tax ID mappings of structure dict[parent_id]=[child1_id, child2_id, ...]
    :param set_of_taxids: Set of Tax IDs to which to add all new Tax IDs
    :return: None
    """

    set_of_taxids.add(taxid)
    try:
        for child_id in taxid_map_dict[taxid]:
            add_taxids_recursively(child_id, taxid_map_dict, set_of_taxids)
    except KeyError:
        pass  # This means the current taxid has no more children


def translate_taxid_set_to_gi_set(taxids, gi_taxid):
    """
    Generates a set of GI numbers matching a given set of tax IDs.
    
    :param taxids: Set of Tax IDs
    :param gi_taxid: String path to gi_taxid_X.dmp file
    :return: Set of GI numbers
    """

    counter = 0
    saved_counter = 0
    gis = set()

    with open(gi_taxid) as infile:
        for line in infile:
            counter += 1
            line_split = line.rstrip().split()
            gi = line_split[0].strip()
            taxid = line_split[1].strip()

            if taxid in taxids:
                gis.add(gi)
                saved_counter += 1

            if counter%1000 == 0:
                sys.stderr.write("Processed {} GIs, saved {} GIs ...\r".format(counter, saved_counter))

    sys.stderr.write("Processed {} GIs, saved {} GIs ...\n".format(counter, saved_counter))

    return gis


def extract_entries_from_db(database, gis):
    """
    Function that extracts all entries from a database fasta that have a GI number contained in a given set of GIs.

    :param database: String path to nt or nr database fasta
    :param gis: Set of GI numbers
    :return: None
    """

    counter = 0
    written_counter = 0
    gen = fasta_file_sequence_generator(database)

    for seq in gen:
        counter += 1
        names = seq[0].split("\x01")  # NCBI puts several annotations in one line separated by this ...
        gis_for_entry = [x.split("|")[1] for x in names]
        for gi in gis_for_entry:
            if gi in gis:
                write_fasta_sequence(sys.stdout, seq)
                written_counter += 1
                break

        if counter%1000==0:
            sys.stderr.write("Processed {} entries, wrote {} entries ...\r".format(counter, written_counter))

    sys.stderr.write("Processed {} entries, wrote {} entries ...\n".format(counter, written_counter))


def write_fasta_sequence(file_handler, seq):
    """
    Short wrapper for writing sequences in fasta format to a file
    :param file_handler: File handle object for a file to write to
    :param seq: List of format ["name (without leading >)", "sequence"]
    :return: None
    """

    file_handler.write(">")
    file_handler.write(seq[0])
    file_handler.write("\n")
    file_handler.write(seq[1])
    file_handler.write("\n")
    #file_handler.flush()


def fasta_file_sequence_generator(inputfile):
    """
    Function that returns a generator that yields fasta format reads as [readname (without the leading ">"), sequence]

    :param inputfile: String path to input fasta file
    :return: Generator object
    """

    infile = open(inputfile)

    # check if file is actually fasta (simple check for ">" as first character in file)
    if not infile.read(1) == ">":
        raise FileFormatException(infile.name + " does not seem to be a valid fasta file")
    infile.seek(0)

    # actual generator stuff
    seq_id = None
    seq = []

    for line in infile:
        if line[0] == ">":
            if seq_id:
                yield [seq_id, "".join(seq)]
            seq_id = line.rstrip()[1:]  # omitting the leading >
            seq = []
        elif line[0] == ";":  # commentary lines are ignored
            continue
        else:
            seq.append(line.rstrip())

    if seq_id:  # don't forget about the last chunk
        yield [seq_id, "".join(seq)]


if __name__ == "__main__":
    main()
