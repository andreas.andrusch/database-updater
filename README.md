# Database Updater  
  
This project includes Python 2 script files intended to be run as a cron job in order to download NCBI's nt and nr databases and making customized versions of them, as well as indexing those for BLAST and Bowtie2.  
  
## Database structure  
  
### NCBI native format  
  
The NCBI native sequence annotation format is as follows for sequences annotated with only one GI:  
`>gi|9|emb|X68321.1| B.taurus mRNA for cyclin A 9`  
and for sequences with several GIs:  
`>gi|39|emb|X57170.1| B.taurus 5S rRNA gene^Agi|675970122|pdb|4W1Z|7 Chain 7, Structure Of The Mammalian 60s Ribosomal Subunit (this Entry Contains The Large Ribosomal Subunit Rna)^Agi|675970173|pdb|4W21|7 Chain 7, Structure Of The 80s Mammalian Ribosome Bound To Eef2 (this Entry Contains The Large Ribosomal Subunit Rna)^Agi|675970264|pdb|4W24|7 Chain 7, Structure Of The Idle Mammalian Ribosome-sec61 Complex (this Entry Contains The Large Ribosomal Subunit Rna)^Agi|675970318|pdb|4W26|7 Chain 7, Structure Of The Translating Mammalian Ribosome-sec61 Complex (this Entry Contains The Large Ribosomal Subunit Rna)`  
which means, that in general the entries will be structured like this:  
`gi|<GI number>|<Other database abbreviation>|<Accession number>|<Description>`  
with the standard > starting a sequence annotation line and the entries separated by \^A on the same line.  
  
### Custom format  
  
The customized database format is as follows:  
`>9|species:9913:Bos_taurus|genus:9903:Bos|subfamily:27592:Bovinae|family:9895:Bovidae|infraorder:35500:Pecora|suborder:9845:Ruminantia|no_rank:91561:Cetartiodactyla|superorder:314145:Laurasiatheria|no_rank:1437010:Boreoeutheria|no_rank:9347:Eutheria|no_rank:32525:Theria|class:40674:Mammalia|no_rank:32524:Amniota|no_rank:32523:Tetrapoda|no_rank:1338369:Dipnotetrapodomorpha|no_rank:8287:Sarcopterygii|no_rank:117571:Euteleostomi|no_rank:117570:Teleostomi|no_rank:7776:Gnathostomata|no_rank:7742:Vertebrata|subphylum:89593:Craniata|phylum:7711:Chordata|no_rank:33511:Deuterostomia|no_rank:33213:Bilateria|no_rank:6072:Eumetazoa|kingdom:33208:Metazoa|no_rank:33154:Opisthokonta|superkingdom:2759:Eukaryota|no_rank:131567:cellular_organisms|no_rank:1:root`  
of for sequences with more than one annotation:  
`>39,675970122,675970173,675970264,675970318|no_rank:91561:Cetartiodactyla|superorder:314145:Laurasiatheria|no_rank:1437010:Boreoeutheria|no_rank:9347:Eutheria|no_rank:32525:Theria|class:40674:Mammalia|no_rank:32524:Amniota|no_rank:32523:Tetrapoda|no_rank:1338369:Dipnotetrapodomorpha|no_rank:8287:Sarcopterygii|no_rank:117571:Euteleostomi|no_rank:117570:Teleostomi|no_rank:7776:Gnathostomata|no_rank:7742:Vertebrata|subphylum:89593:Craniata|phylum:7711:Chordata|no_rank:33511:Deuterostomia|no_rank:33213:Bilateria|no_rank:6072:Eumetazoa|kingdom:33208:Metazoa|no_rank:33154:Opisthokonta|superkingdom:2759:Eukaryota|no_rank:131567:cellular_organisms|no_rank:1:root`  
or in general:  
`><GI Number OR comma separated list of GI Numbers>|<taxonomic nodes from leaf to root>`  
with each taxonomic node structured like this  
`<rank of the node>:<TaxID of the node/organism>:<Name of the node/organism>`  
  
### Combined/conservative format  
  
The database format retaining the native NCBI information is basically both formats in one annotation line.  
Although in this format the NCBI annotation is altered to be SAM file format compliant, meaning all annotations (NCBI annotations and taxonomic path annotation) are separated by `||` instead of the NCBI native separation character and all spaces in the NCBI annotations are replaced by underscores. 
For example:  
`>gi|9|emb|X68321.1|_B.taurus_mRNA_for_cyclin_A_9|species:9913:Bos_taurus|genus:9903:Bos|subfamily:27592:Bovinae|family:9895:Bovidae|infraorder:35500:Pecora|suborder:9845:Ruminantia|no_rank:91561:Cetartiodactyla|superorder:314145:Laurasiatheria|no_rank:1437010:Boreoeutheria|no_rank:9347:Eutheria|no_rank:32525:Theria|class:40674:Mammalia|no_rank:32524:Amniota|no_rank:32523:Tetrapoda|no_rank:1338369:Dipnotetrapodomorpha|no_rank:8287:Sarcopterygii|no_rank:117571:Euteleostomi|no_rank:117570:Teleostomi|no_rank:7776:Gnathostomata|no_rank:7742:Vertebrata|subphylum:89593:Craniata|phylum:7711:Chordata|no_rank:33511:Deuterostomia|no_rank:33213:Bilateria|no_rank:6072:Eumetazoa|kingdom:33208:Metazoa|no_rank:33154:Opisthokonta|superkingdom:2759:Eukaryota|no_rank:131567:cellular_organisms|no_rank:1:root`  
`>gi|39|emb|X57170.1|_B.taurus_5S_rRNA_gene||gi|675970122|pdb|4W1Z|7_Chain_7,_Structure_Of_The_Mammalian_60s_Ribosomal_Subunit_(this_Entry_Contains_The_Large_Ribosomal_Subunit_Rna)||gi|675970173|pdb|4W21|7_Chain_7,_Structure_Of_The_80s_Mammalian_Ribosome_Bound_To_Eef2_(this_Entry_Contains_The_Large_Ribosomal_Subunit_Rna)||gi|675970264|pdb|4W24|7_Chain_7,_Structure_Of_The_Idle_Mammalian_Ribosome-sec61_Complex_(this_Entry_Contains_The_Large_Ribosomal_Subunit_Rna)||gi|675970318|pdb|4W26|7_Chain_7,_Structure_Of_The_Translating_Mammalian_Ribosome-sec61_Complex_(this_Entry_Contains_The_Large_Ribosomal_Subunit_Rna)_39,675970122,675970173,675970264,675970318|no_rank:91561:Cetartiodactyla|superorder:314145:Laurasiatheria|no_rank:1437010:Boreoeutheria|no_rank:9347:Eutheria|no_rank:32525:Theria|class:40674:Mammalia|no_rank:32524:Amniota|no_rank:32523:Tetrapoda|no_rank:1338369:Dipnotetrapodomorpha|no_rank:8287:Sarcopterygii|no_rank:117571:Euteleostomi|no_rank:117570:Teleostomi|no_rank:7776:Gnathostomata|no_rank:7742:Vertebrata|subphylum:89593:Craniata|phylum:7711:Chordata|no_rank:33511:Deuterostomia|no_rank:33213:Bilateria|no_rank:6072:Eumetazoa|kingdom:33208:Metazoa|no_rank:33154:Opisthokonta|superkingdom:2759:Eukaryota|no_rank:131567:cellular_organisms|no_rank:1:root`  
  
### Restoring the NCBI native format from the combined/conservative format  
  
If the updater is setup to create databases of the conservative format combination the untouched databases are not saved to preserve space. If they are specifically needed, they can be restored by running sed as follows:  
`sed -e 's/||[^|]*$//g' /path/to/combined_format_database.fa > /path/to/restored_ncbi_native_database.fa`  
This will leave the NCBI annotations separated by `||` instead of the native non printable character and will also leave all underscores in place.  

`sed -e 's/||[^|]*$//g' /path/to/combined_format_database.fa | sed 's/||/\x01/g' > /path/to/restored_ncbi_native_database.fa`  
This will restore the annotation separators as well.  

`sed -e 's/||[^|]*$//g' /path/to/combined_format_database.fa | sed 's/||/\x01/g' | sed 's/_/ /g' > /path/to/restored_ncbi_native_database.fa`  
This will replace underscores with spaces again.  
Note that this will replace all originally occuring underscores with spaces as well, thus not being completely equal to the original NCBI database.  
 
### Preparing subsets of the database  
  
If a subset of the nt or the various smaller databases is desired it can easily be created using grep as follows:  
`grep -A 1 ":<TaxID>:" /path/to/database.fa | grep -v "^--$" > /path/to/subset_database.fa`  
where `<TaxID>` has to be substituted with the taxonomic ID of the node/organism (group) desired.  
These can be easily obtained by searching the database itself or under http://www.ncbi.nlm.nih.gov/taxonomy  
  
The second grep command is there to eliminate separator lines that can be introduced by the first command.  
If this subset is desired in the NCBI native format this can be achieved by running grep on the combined format database and then running the sed command from above on the created subset of sequences.  
For the extraction from the NCBI native format itself, the script extract_sequences.py can be used, due to speed limitations and memory usage, this is discouraged though.  
  
### Folder structure  
  
The updater will work in the directory given in the config file and retain the taxonomy files used in the creation of the newest database there.  
It will also create a subfolder named 1 or 2 for the newest version of the database itself, depending on which of those is currently in use by the most recent version of the database.  
In these folders it will prepare subfolders for the nt and nr database named nt and nr repectively.  
Under these it will create a subfolder for each desired subset specified in the config, e.g. viruses, bacteria, homo_sapiens etc.  
It will also create a folder named parts, where it splits the nt database to parts of equal size for special use cases.  
Inside of the subset folders it will create a BLAST folder containing the BLAST index and a folder called index containing the Bowtie2 index.  
Once the database is fully created and indexed the 1 or 2 folder respectively will be linked to the location specified in the config and the newest database version will therefore always be reachable under the same path.  
The older version of the database will then be deleted.  
  
Example tree:  
  
. (taxonomy files reside here)  
|-1  
|---nt (nt.fa - the converted database and possibly nt\_unprocessed.fa - the native NCBI format database reside here)  
|-----bacteria (bacteria.fa resides here)  
|-------BLAST (bacteria BLAST index resides here)  
|-------index (Bowtie2 index resides here)  
|-----homo_sapiens  
|-------BLAST  
|-------index  
|-----parts (optional; equal sized parts of the database reside here, no Bowtie2 indices are created for these)  
|-------BLAST  
|-----viruses  
|-------BLAST  
|-------index  
