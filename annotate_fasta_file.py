#!/usr/bin/env python

import database_updater
import sys

if len(sys.argv) != 6:
    exit("Usage: python {} <input.fasta> <output.fasta> <nodes.dmp> <names.dmp> <gi_taxid_nucl.dmp>".format(sys.argv[0]))
else:
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    nodes = sys.argv[3]
    names = sys.argv[4]
    gi_to_taxid = sys.argv[5]
    database_updater.convert_fasta_file_to_our_format(input_file, output_file, nodes, gi_to_taxid, names)
