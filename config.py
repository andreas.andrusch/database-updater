"""
Config file for the database_updater.py script
"""

# proxy settings if required / set to empty strings if no proxy settings apply
http_proxy = ''
https_proxy = ''
ftp_proxy = ''

# set the databases to download and process
# the databases are split into 4 groups
# 1. nucleic acid blast databases
# 2. protein blast databases
# 3. nucleic acid fasta databases (complete databases in 1 file each)
# 4. protein fasta databases (complete databases in 1 file each)
# in each dictionary, the keys are the names of databases to download
# refer to ftp://ftp.ncbi.nlm.nih.gov/blast/db/ for the blast databases
# and to ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA for the fasta databases
# the value is a perl style regular expression string that is used to select sequences from each database
# to select all sequences use '.' (the regular expression that matches any character)
nucl_blast_databases = {
    # filter for specific accessions - NZ is kept out to keep the bacteria db small
    'refseq_genomic': 'N[CGMRTW]_\d+\.\d+',
    # rough filter to keep short sequences from the nt out - only including the following matches
    'nt': 'complete|partial|sequence|region|genom|chromosom|cds|gene|protein|virus|phage|strain|mrna'
}

prot_blast_databases = {}

nucl_fasta_databases = {}

prot_fasta_databases = {}

# conserve the ncbi native sequence annotation and add our own format after the annotation separator OR use ONLY our
# format if this option is activated, unprocessed databases are not kept, since they are easily recreatable from the
# processed ones
conserve_ncbi_names = True

# set whether sequences annotated to the taxonomic root or sequences containing only Ns should be skipped
skip_root_only_sequences = False  # sometimes discrepancies between data dumps; with false we keep at least accessions
skip_n_only_sequences = True

# directories
# this is where the completely processed databases will be linked to
link_directory = '/databases/dynamic'
# this is where the updater will download and prepare the dbs
working_directory = '/.database_updates'

# taxonomies for which to create subset databases
# list [str name_for_the_db, int taxid]
taxonomies = [
    ['homo_sapiens', 9606],
    ['bacteria', 2],
    ['viruses', 10239],
    ['fungi', 4751],
    ['amoebozoa', 554915],
    ['apicomplexa', 5794],
    # ['artificial_sequences', 81077],  # these are not contained in the refseq database
    # ['primates', 9943],
    # ['vertebrates', 7742],
]

# number of threads to use for threadable operations, including unpacking and indexing
# don't set indexing threads too high as parallel indexing can lead to a high RAM consumption
# all other threaded operations are not RAM intensive and possibly safely use a higher number of threads
indexing_threads = 12
other_threads = 12

# our annotation format separators
info_separator = ':'  # used to separate info of the same taxonomic node, e.g. rank and name
chunk_separator = '|'  # used to separate taxonomic nodes, e.g. a node (e.g. genus) and its child (e.g. species)
annotation_separator = '||'  # used to separate complete annotations/deflines, e.g. several NCBI annotations and/or ours

# set whether to delete or keep other, older databases created by this script once it is done creating the new db
# if not deleted the old versions are merely unlinked
delete_other_database_versions = True

# set the gzip compression level to use (should be between 1 and 9, default is 6)
compression_level = 6

# set custom paths for programs that the updater depends on
# if these are not set the script will try to find the programs using the PATH variable
# the tool 'which' has to be available in that case to determine not filled paths
dependencies = {
    'awk': None,
    'blastdbcmd': None,
    'bowtie2-build': None,
    'cut': None,
    'grep': None,
    'makeblastdb': None,
    'md5sum': None,
    'pigz': None,
    'tar': None,
    'wget': None,
    'zcat': None
}

# set the logging level to use during run
logging_level = 'DEBUG'
